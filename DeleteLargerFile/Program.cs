﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace DeleteLargerFile
{
    class Program
    {
        class FilePair
        {
            public FileInfo file1 {get; set; }
            public FileInfo file2 { get; set; }

            public bool isOK()
            {
                return (file1 != null) && (file2 != null);
            }
        }
        static void Main(string[] args)
        {
            if (args.Length < 3)
            {
                Console.WriteLine("Please use this format : {folder path} {ext1} {ext2}");
            }
            DirectoryInfo folderAll =
            new DirectoryInfo(args[0]);
            var files = getAllFiles(folderAll);
            var allPair = new Dictionary<string, FilePair>();
            int extLength = Math.Max(args[1].Length, args[2].Length);
            foreach (var file in files)
            {
                var fullNameNoExt = file.FullName.Substring(0, file.FullName.Length - extLength);
                FilePair pair;
                if(allPair.TryGetValue(fullNameNoExt, out pair))
                {
                    if (file.Extension.Contains(args[1]))
                    {
                        pair.file1 = file;
                    }
                    else if (file.Extension.Contains(args[2]))
                    {
                        pair.file2 = file;
                    }
                }else
                {
                    if (file.Extension.Contains(args[1]))
                    {
                        allPair.Add(fullNameNoExt, new FilePair { file1 = file });
                    }
                    else if (file.Extension.Contains(args[2]))
                    {
                        allPair.Add(fullNameNoExt, new FilePair { file2 = file });
                    }
                }
            }
            foreach (var pair in allPair.Values)
            {
                if (!pair.isOK()) continue;
                if (pair.file1.Length < pair.file2.Length)
                {
                    Console.WriteLine(pair.file2.Name + " is deleted!");
                    pair.file2.Delete();
                }
                else
                {
                    Console.WriteLine(pair.file1.Name + " is deleted!");
                    pair.file1.Delete();
                }
            }
            Console.WriteLine("Done!");
            Console.ReadKey();
        }

        static IList<FileInfo> getAllFiles(DirectoryInfo di)
        {
            List<FileInfo> files = di.GetFiles().ToList();
            foreach (var dir in di.GetDirectories())
            {
                files.AddRange(getAllFiles(dir));
            }
            return files;
        }
    }
}
